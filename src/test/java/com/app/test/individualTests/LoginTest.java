package com.app.test.individualTests;

import com.app.framework.utilities.ExcelUtil;
import com.app.test.pages.DashboardPage;
import com.app.test.pages.LoginPage;
import com.app.test.steps.TestInitialize;

public class LoginTest extends TestInitialize {

    /*@Test*/
    public void LoginEticket() throws InterruptedException{
        CurrentPage = GetInstance(LoginPage.class);
        CurrentPage = CurrentPage.As(LoginPage.class).Login(ExcelUtil.ReadCell("ruc",1),ExcelUtil.ReadCell("user",1), ExcelUtil.ReadCell("password",1));
        CurrentPage = CurrentPage.As(DashboardPage.class).Logout();
    }
}