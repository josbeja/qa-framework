package com.app.test.individualTests;

import com.app.framework.base.BrowserType;
import com.app.framework.base.DriverContext;
import com.app.framework.base.FrameworkInitialize;
import com.app.test.pages.DashboardPage;
import com.app.test.pages.DocumentsPage;
import com.app.test.pages.LoginPage;
import com.app.framework.utilities.ExcelUtil;

public class MassiveCreationByCSVTest extends FrameworkInitialize {

    /*@Before*/
    public void Initialize(){

        InitializeBrowser(BrowserType.Firefox);
        //DriverContext.Browser.GoToUrl("http://eticket.redbus.com/login");
        DriverContext.WaitForPageToLoad();

        try {
            ExcelUtil util = new ExcelUtil("C:\\Users\\Jose Bejarano\\Documents\\GitProjects\\NewAutomationProject\\data.xls");
        }catch (Exception e){
            System.out.println(e);
        }

    }

    /*@Test*/
    public void MassiveCreationByCSV() throws InterruptedException{
        //CurrentPage = GetInstance(LoginPage.class);
        //CurrentPage = CurrentPage.As(LoginPage.class).Login("00000000000","jose.bejarano", "~ms?t*?24vM(.<Dp");
        CurrentPage.As(LoginPage.class).Login(ExcelUtil.ReadCell("ruc",1),ExcelUtil.ReadCell("user",1), ExcelUtil.ReadCell("password",1));
        CurrentPage.As(DashboardPage.class).itemDocuments.click();
        CurrentPage.As(DocumentsPage.class).buttonCSV.click();
    }
}
