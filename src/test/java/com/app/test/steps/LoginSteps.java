package com.app.test.steps;

import com.app.framework.base.Base;
import com.app.framework.base.DriverContext;
import com.app.framework.utilities.CucumberUtil;
import com.app.test.pages.DashboardPage;
import com.app.test.pages.LoginPage;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;

import java.util.List;

public class LoginSteps extends Base {
    
    @Given("I ensure the application is opened")
    public void iEnsureTheApplicationIsOpened() throws ClassNotFoundException {
        CurrentPage = GetInstance(LoginPage.class);
        DriverContext.WaitForPageToLoad();
        Assert.assertTrue("The Login Page is not loaded", CurrentPage.As(LoginPage.class).isLogin());
    }

    @Then("I enter Ruc, User and Password")
    public void iEnterRucUserAndPassword(DataTable data) throws ClassNotFoundException {
        CucumberUtil.ConvertDataTableToDict(data);
        CurrentPage.As(LoginPage.class).fillLoginCredentials(
                CucumberUtil.GetCellValueWithRowIndex("Ruc",1),
                CucumberUtil.GetCellValueWithRowIndex("UserName",1),
                CucumberUtil.GetCellValueWithRowIndex("Password",1)
        );

//        ExtentReport.getScenario().createNode(new GherkinKeyword("Then"), "I enter Ruc, User and Password");
    }

    @Then("I click login button \\(success login)")
    public void iClickLoginButtonSuccessLogin() {
        CurrentPage = CurrentPage.As(LoginPage.class).clickLoginSuccess();
        DriverContext.WaitForPageToLoad();
    }

    @And("I ensure the Dashboard is opened")
    public void iEnsureTheDashboardIsOpened() {
        Assert.assertTrue("Dashboard is not loaded", CurrentPage.As(DashboardPage.class).isDashboardLoad());
    }

    @Then("I should see the dashboard page with the user name")
    public void iShouldSeeTheDashboardPageWithTheUserName(DataTable data) {
        List<List<String>> table = data.cells();
        DriverContext.WaitForElementTextVisible(CurrentPage.As(DashboardPage.class).itemUsername,CurrentPage.As(DashboardPage.class).itemUsername.getText());
        Assert.assertEquals("User is not logged",CurrentPage.As(DashboardPage.class).getUserLogged(),data.cell(1,0));
    }

    @Then("I click login button \\(fail login)")
    public void iClickLoginButtonFailLogin() {
        CurrentPage = CurrentPage.As(LoginPage.class).clickLoginFail();
        DriverContext.WaitForPageToLoad();
    }

    @And("I should see the error message {string}")
    public void checkErrorMessageLogin(String errorMessage) {
        Assert.assertEquals(errorMessage,CurrentPage.As(LoginPage.class).getErrorMessage());
    }
}
