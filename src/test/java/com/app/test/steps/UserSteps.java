package com.app.test.steps;

import com.app.framework.base.Base;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;


public class UserSteps extends Base {

    @Then("I click Users link")
    public void iClickUsersLink() {
    }

    @Then("I click new user button")
    public void iClickNewUserButton() {
    }

    @Then("I select the test company {string}")
    public void iSelectTheTestCompany(String arg0) {
    }

    @And("I enter the new user, password, confirm password")
    public void iEnterTheNewUserPasswordConfirmPassword() {
    }

    @Then("I click Save button")
    public void iClickSaveButton() {
    }

    @Then("I verify if the new user was created")
    public void iVerifyIfTheNewUserWasCreated() {
    }
}
