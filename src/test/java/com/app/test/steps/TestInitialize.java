package com.app.test.steps;

import com.app.framework.utilities.LogUtil;
import com.app.framework.base.DriverContext;
import com.app.framework.base.FrameworkInitialize;
import com.app.framework.config.ConfigReader;
import com.app.framework.config.Settings;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import net.masterthought.cucumber.json.support.Status;
import net.masterthought.cucumber.presentation.PresentationMode;
import net.masterthought.cucumber.sorting.SortingMethod;
import org.testng.annotations.AfterSuite;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TestInitialize extends FrameworkInitialize {

    @Before
    public void Initialize() throws IOException {

        //Insert scenario name
        //ExtentReport.startScenario(scenario.getName());

        //Initialize config
        ConfigReader.PopulateSettings();

        //Logging
        Settings.Logs = new LogUtil();
        Settings.Logs.CreateLogFile();
        Settings.Logs.Write("Framework Initialized");

        InitializeBrowser(Settings.BrowserType);
        Settings.Logs.Write("Browser Initialized");
        DriverContext.Maximize();
        DriverContext.GoToUrl(Settings.AUT);
        DriverContext.WaitForPageToLoad();
        Settings.Logs.Write("Navigate to: " + Settings.AUT);
    }

    @After
    public void afterTests() throws IOException{
        DriverContext.Quit();
    }

}
