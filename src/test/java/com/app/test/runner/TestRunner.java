package com.app.test.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@BecaPresidenteDeLaRepublica"},
        strict = true,
        features = {"src/test/java/com/app/test/features"},
        glue = {"com.app.test.steps"},
        monochrome = true,
        plugin = {"pretty",
                "html:target/cucumber-reports",
                "json:target/cucumber-reports/cucumber.json",
                "de.monochromata.cucumber.report.PrettyReports:target/cucumber-reports"
        })
public class TestRunner{
}
