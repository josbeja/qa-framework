package com.app.test.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@testScenario"},
        strict = true,
        features = {"src/test/java/com/rb/test/features"},
        glue = {"com.rb.test.steps"},
        monochrome = true,
        plugin = {"pretty",
                "html:target/cucumber-reports",
                "json:target/cucumber-reports/cucumber.json"})
public class TestRunnerDev {
}