@testScenario
Feature: testFeature
  This feature is responsible for testing new User creation, edit user, deactivate user, activate user, promove user, degrade user

  Background:
    Given I ensure the application is opened
    Then I enter Ruc, User and Password
      |Ruc|UserName|Password|
      |11111111111|admin|admin|
    Then I click login button (success login)
    And I ensure the Dashboard is opened

  Scenario: Scenario1-User
    Then I click Users link