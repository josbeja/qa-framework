@UserFeatures
Feature: Feature2-UserFeature
  This feature is responsible for testing new User creation, edit user, deactivate user, activate user, promove user, degrade user

  Background:
    Given I ensure the application is opened
    Then I enter Ruc, User and Password
      |Ruc|UserName|Password|
      |11111111111|admin|admin|
    Then I click login button (success login)
    And I ensure the Dashboard is opened

  Scenario: Scenario1-User
    Then I click Users link
    Then I click new user button
    #Then I select the test company "uno"
    Then I enter the new user, password, confirm password
    And I click Save button
    Then I verify if the new user was created

  Scenario: Scenario2-User
    Then I click Users link
    Then I click new user button
    #Then I select the test company "uno"
    Then I enter the new user, password, confirm password
    And I click Save button
    Then I verify if the new user was created