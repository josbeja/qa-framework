@LoginFeatures
Feature: LoginFeature
  This feature is responsible for testing all the scenarios for Login.

  Background:
    Given I ensure the application is opened

  Scenario: Login with valid credentials
    Then I enter Ruc, User and Password
      |Ruc|UserName|Password|
      |11111111111|admin|admin|
    Then I click login button (success login)
    And I ensure the Dashboard is opened
    Then I should see the dashboard page with the user name
      |UserName|
      |admin|

  Scenario: Login with invalid credentials
    Then I enter Ruc, User and Password
      |Ruc|UserName|Password|
      |11111111111|admin|noadmin|
    Then I click login button (fail login)
    And I should see the error message "Credenciales no válidas."