@LoginFeatures
Feature: CheckDashboardItems
  Verify if all items of dashboard are successful loaded

  Background:
    Given I ensure the application is opened
    Then I enter Ruc, User and Password
      |Ruc|UserName|Password|
      |11111111111|admin|admin|
    Then I click login button (success login)
    And I ensure the Dashboard is opened

  Scenario: Verify TOTAL documents
    Then I verify if TOTAL is displayed

  Scenario: Verify FACTURAS PENDIENTES PARA CANCELAR
    Then I verify if FACTURAS PENDIENTES PARA CANCELAR is displayed

  Scenario: Verify BOLETAS PENDIENTES
    Then I verify if BOLETAS PENDIENTES is displayed

  Scenario: Verify BOLETAS PENDIENTES PARA CANCELAR
    Then I verify if BOLETAS PENDIENTES PARA CANCELAR is displayed

  Scenario: Verify Resumen por día
    Then I verify if Resumen por día is displayed

  Scenario: Verify Resumen por tipo
    Then I verify if Resumen por tipo is displayed

  Scenario: Verify Resumen por estado
    Then I verify if Resumen por estado is displayed

  Scenario: Verify Notificaciones
    Then I verify if Notificaciones is displayed
