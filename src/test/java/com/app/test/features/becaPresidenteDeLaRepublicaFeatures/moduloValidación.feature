@BecaPresidenteDeLaRepublica
Feature: Modulo de validación
  Módulo de Validación de Expedientes presentados en la
  Fase de Postulación de la Beca Presidente de la República - Convocatoria 2020

  Background:
    Given La URL de prueba
    Then Se carga la pagina de prueba
      |URL|
      |https://prueba10.pronabec.gob.pe/|

  Scenario: Validar cabecera información general
    Given Se ingresa con el usuario y contraseña
      |usuario|contraseña|
      |48318579|Pronabec2019|
    Then Se verifica la cabecera de Información General
      |Cabecera|
      |N° de expediente (generado)|
      |Número de DNI              |
      |Apellidos y Nombres        |
      |Sexo                       |
      |Lengua Materna             |
      |Dirección según RENIEC     |
      |Domicilio actual           |
      |Edad a la fecha de publicación de las bases|
      |Comunidad o Población a la que pertenece (de ser el caso)|
      |Región, provincia y distrito de la comunidad o población a la que pertenecer (de ser el caso)|
      |Tipo y sub tipo de discapacidad (de ser el caso)                                             |

  Scenario: Validar Información del programa al que postula
    Given Se ingresa con el usuario y contraseña
      |usuario|contraseña|
      |48318579|Pronabec2019|
    Then Se verifica la cabecera de Información General
      |Tipo de Programa|
      |País de destino |
      |IES de destino  |
      |Nombre del programa de destino|
      |Area de conocimiento          |
      |Costo del programa            |
      |Duración del programa         |
      |Fecha de inicio y fin de los estudios|

  Scenario: Validar que se muestren requisitos de documentos obligatorios
    Given Usuario y contraseña de un usuario con el perfil validador
      |usuario|contraseña|
      |48318579|Pronabec2019|
    Then Se verifica los documentos obligatorios
      |documentos obligatorios|
      |Estudios de pregrado o posgrado|
      |Rendimiento académico|
      |Experiencia laboral|
      |Buen perfil profesional y/o de investigación a|
      |Buen perfil profesional y/o de investigación b|
      |Carta de aceptación definitiva de una IES Elegible|
      |Ensayos de postulación 1|
      |Ensayos de postulación 2|
      |Insuficientes recursos económicos|
      |No tener impedimentos de postulación|

    Then Se verifica los documentos opcionales

  Scenario: Validar que se muestren requisitos de documentos opcionales
    Given Usuario y contraseña de un usuario con el perfil validador
      |usuario|contraseña|
      |48318579|Pronabec2019|
    Then Se verifica los documentos opcionales
      |documentos opcionales|
      |Víctima de la Violencia|
      |(DI) Carné o Resolución emitido por CONADIS para interesados que declaren poseer algún tipo de discapacidad|
      |(IA) Pertenencia a Comunidad Nativa Amazónica o Comunidad Campesina o Población afroperuanas|
      |(B) Constancia emitida por el Cuerpo General de Bomberos Voluntarios del Perú               |
      |(V) Certificado emitido por el Registro de Voluntarios del Ministerio de la Mujer y Poblaciones Vulnerables|
      |Resolución de aprobación de tesis para optar por el Título Profesional Universitario                       |
      |Documento que acredite contar con una Beca Académica                                                       |

  Scenario: Validar Estudios de pregrado o posgrado para quienes no cuenten con registro en SUNEDU (Maestría)
    Given Usuario y contraseña de un usuario con el perfil validador
      |usuario|contraseña|
      |48318579|Pronabec2019|
    Then Información académica se muestra debajo de la cabecera junto a los documentos cargados en dicha sección
    Then Validar Grado académico de bachiller o título profesional o título técnico profesional

  Scenario: Validar Estudios de pregrado o posgrado para quienes no cuenten con registro en SUNEDU (Doctorado)
    Given Usuario y contraseña de un usuario con el perfil validador
      |usuario|contraseña|
      |48318579|Pronabec2019|
    Then Información académica se muestra debajo de la cabecera junto a los documentos cargados en dicha sección
    Then Validar Grado académico de bachiller o licenciatura o título técnico profesional o maestría

  Scenario: El validador puede corregir los campos registrados por el postulante de la sección Información Académica
    Given Usuario y contraseña de un usuario con el perfil validador
      |usuario|contraseña|
      |48318579|Pronabec2019|
    Then Información académica se muestra debajo de la cabecera junto a los documentos cargados en dicha sección
    Then Se da click en el boton EDITAR EXP.
    Then Se carga un popup con información del postulando disponible para editar
    And Se verifica que la información a editar sea la siguiente:
      |información|
      |Grado O Título|
      |IES           |
      |Tipo De Gestión|
      |Carrera / Especialidad|
      |Fecha de Obtención Grado|
      |Fecha de Emisión del Diploma|
      |Ciudad / País               |

  Scenario: El validador corrige los campos registrados por el postulante de la sección Información Académica
    Given Usuario y contraseña de un usuario con el perfil validador
      |usuario|contraseña|
      |48318579|Pronabec2019|
    Then Información académica se muestra debajo de la cabecera junto a los documentos cargados en dicha sección
    Then Se da click en el boton EDITAR EXP.
    Then Se carga un popup con información del postulando disponible para editar
    And Se editar información registrada por el postulando
    And Se da click en el boton grabar
    Then Se verifica la nueva informacióna actualizada

  Scenario: El validador debe verificar obligatoriedad de una opción para Estudios de pregrado o posgrado
    Given Usuario y contraseña de un usuario con el perfil validador
      |usuario|contraseña|
      |48318579|Pronabec2019|
    Then Verifica que se seleccione como obligatoria una de las siguientes opciones:
      |opciones|
      |a. Título Técnico Profesional o Pedagógico|
      |b. Grado de Bachiller|
      |c. Título Profesional|
      |d. Grado de Maestro|

  Scenario: El validador debe verificar obligatoriedad de una opción para Rendimiento académico
    Given Usuario y contraseña de un usuario con el perfil validador
      |usuario|contraseña|
      |48318579|Pronabec2019|
    Then Verifica que se seleccione como obligatoria una de las siguientes opciones:
      |opciones|
      |a. Décimo superior, primer o segundo puesto|
      |b. Quinto superior                         |
      |c. Tercio superior                         |

  Scenario: El validador corrige la experiencia laboral registrada por el postulante
    Given Usuario y contraseña de un usuario con el perfil validador
      |usuario|contraseña|
      |48318579|Pronabec2019|
    Then Experiencia laboral se muestra debajo de la cabecera junto a los documentos cargados en dicha sección
    Then Se da click en el boton EDITAR EXP
    Then Se carga un popup con información del postulando disponible para editar
    And El validador edita los siguientes campos:
      |campos|
      |Nombre de la entidad o empresa|
      |Tipo de gestión               |
      |Cargo desempeñado             |
      |Fecha de inicio y fin del tiempo laborado|
      |Agregar o eliminar registros de experiencia laboral|
    Then Se da click en el boton grabar
    And Se verifica la nueva informacióna actualizada
    And Se verifica que se recalcular de manera automática el tiempo laborado por el tipo de gestión y el total general del tiempo de experiencia calculado en meses y días.

  Scenario: Validar buen perfil profesional y/o de investigación
    Given Usuario y contraseña de un usuario con el perfil validador
      |usuario|contraseña|
      |48318579|Pronabec2019|
    Then El modulo muestra los ítems por separado y sólo los que el postulante haya cargado en la fase de postulación
