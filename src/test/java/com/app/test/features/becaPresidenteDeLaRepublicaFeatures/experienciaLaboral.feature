@BecaPresidenteDeLaRepublica
Feature: Formulario de Experiencia Laboral
  Experiencia cuente desde la fecha minima de todos sus grados
  #al guardar o editar una experiencia no te debe permitir y tambien en la sumatoria  
  
  Background: Pasos previos
    Given Pantalla de Experiencia Laboral cargada correctamente

  Scenario: Registrar Experiencia con fecha menor al minimo grado
    Given Postulante registra experiencia con fecha menor al minimo grado
    Then Sistema no permite registrar experiencia

  Scenario: Registrar Experiencia con fecha igual al ultimo grado
    Given Postulante registra experiencia con fecha igual al minimo grado
    Then Sistema permite registrar experiencia

  Scenario: Editar Experiencia con fecha menor al ultimo grado
    Given Postulante edita experiencia con fecha menor al minimo grado
    Then Sistema no permite editar experiencia

  Scenario: Editar Experiencia con fecha igual al ultimo grado
    Given Postulante edita experiencia con fecha igual al minimo grado
    Then Sistema permite editar experiencia
  
  Scenario: Registrar Experiencia con fecha mayor al ultimo grado
    Given Postulante registra experiencia con fecha mayor al minimo grado
    Then Sistema permite editar experiencia

  Scenario: Editar Experiencia con fecha mayor al ultimo grado
    Given Postulante edita experiencia con fecha mayor al minimo grado
    Then Sistema permite editar experiencia