@BecaPresidenteDeLaRepublica
Feature: Formulario de Información Academica
  Información academica del Postulante y documentación adjunta
  
  Background: Pasos previos
    Given Pantalla de Información academica cargada correctamente

  Scenario: Requisito minimo Bachiller
    Given Postulante registra bachiller como unica información academica
    When Postulante da click en seguir
    Then Sistema permite continuar

  Scenario: Requisito minimo titulo tecnico profesional
    Given Postulante registra titulo tecnico profesional como unica información academica
    When Postulante da click en seguir
    Then Sistema permite continuar

  Scenario: Requisito minimo Titulo Profesional
    Given Postulante registra Titulo Profesional como unica información academica
    When Postulante da click en seguir
    Then Sistema permite continuar

  Scenario: Requisito minimo Licenciatura
    Given Postulante registra Licenciatura como unica información academica
    When Postulante da click en seguir
    Then Sistema permite continuar

  Scenario: Requisito minimo Maestria
    Given Postulante registra Maestria como unica información academica
    When Postulante da click en seguir
    Then Sistema permite continuar

  Scenario: Mas de un registro en información academica
    Given Postulante registra Bachiller como información academica
    And Postulante registra Maestria como información academica
    When Postulante da click en seguir
    Then Sistema permite continuar

  Scenario: Retornar a Editar registro unico una vez cargada experiencia laboral
    Given Postulante vuelve a la pantalla de información academica
    And Postulante edita registro
    Then Sistema no permite editar registro

  Scenario: Retornar a Editar registros con menor fecha una vez cargada experiencia laboral
    Given Postulante vuelve a la pantalla de información academica
    When Postulante edita registro con menor fecha
    Then Sistema no permite editar registro
    Then Postulante editar registro con mayor fecha
    And Sistema permite editar registro

  Scenario: Retornar a eliminar unico registro una vez cargada experienca laboral
    Given Postulante vuelve a la pantalla de información academica
    When Postulante elimina unico registro
    Then Sistema no permite eliminar registro
    And Sistema muestra mensaje de advertencia

  Scenario: Retornar a eliminar registros una vez cargada experienca laboral
    Given Postulante vuelve a la pantalla de información academica
    When Postulante elimina registros exceptuando el de menor fecha
    Then Sistema permite eliminar registros
    And Sistema muestra mensaje confirmando eliminación