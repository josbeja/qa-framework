package com.app.test.pages;

import com.app.framework.base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class DocumentsPage extends BasePage {

    //public DocumentsPage(){}

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'csv')]")
    public WebElement buttonCSV;

}
