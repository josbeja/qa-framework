package com.app.test.pages;

import com.app.framework.base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class UserPage extends BasePage {

    //public UserPage(){}

    @FindBy(how = How.XPATH, using = "//a[@href='/user/new']")
    public WebElement btnNewUser;

    @FindBy(how = How.XPATH, using = "//h1[text()='Usuarios']")
    public WebElement itemUserTittle;

    public boolean isUserPageLoad(){
        boolean isLoad = false;
        if(itemUserTittle.isDisplayed()){
            isLoad = true;
        }
        return isLoad;
    }

    public NewUserPage clickNewUser(){
        btnNewUser.click();
        return GetInstance(NewUserPage.class);
    }



}
