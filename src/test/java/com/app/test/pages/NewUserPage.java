package com.app.test.pages;

import com.app.framework.base.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class NewUserPage extends BasePage {

    //public NewUserPage(){}

    @FindBy(how = How.XPATH, using = "//input[@id='ruc']")
    public WebElement txtRuc;

    @FindBy(how = How.XPATH, using = "//input[@id='email']")
    public WebElement txtUser;

    @FindBy(how = How.XPATH, using = "//input[@id='password']")
    public WebElement txtPassword;

    @FindBy(how = How.XPATH, using = "//button[@type='submit']")
    public WebElement btnLoginButton;

    public DashboardPage Login(String ruc, String user, String password) throws InterruptedException{
        Thread.sleep(1000);
        txtRuc.sendKeys(ruc);
        txtUser.sendKeys(user);
        txtPassword.sendKeys(password);
        Thread.sleep(1000);
        btnLoginButton.submit();
        return GetInstance(DashboardPage.class);
    }

    public void fillLoginCredentials(String ruc, String user, String password){
        txtRuc.sendKeys(ruc);
        txtUser.sendKeys(user);
        txtPassword.sendKeys(password);
    }

    public DashboardPage clickLogin(){
        btnLoginButton.submit();
        return GetInstance(DashboardPage.class);
    }

    public boolean isLogin(){
        boolean isLogin = false;
        if(txtRuc.isDisplayed() & txtPassword.isDisplayed() & txtUser.isDisplayed()){
            isLogin = true;
        }
        return isLogin;
    }

}
