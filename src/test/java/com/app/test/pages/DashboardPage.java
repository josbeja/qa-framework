package com.app.test.pages;

import com.app.framework.base.BasePage;
import com.app.framework.base.DriverContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class DashboardPage extends BasePage {

    public DashboardPage() {}

    @FindBy(how = How.XPATH, using = "//ul[@class='navbar-nav ml-auto']//li[3]")
    public WebElement listSettings;

    @FindBy(how = How.XPATH, using = "//div//li[3]//div[@class='dropdown-menu dropdown-menu-right shadow animated--grow-in show']")
    public WebElement itemLogout;

    @FindBy(how = How.XPATH, using = "//a[@href='/document/']")
    public WebElement itemDocuments;

    @FindBy(how = How.XPATH, using = "//h1[text()='Dashboard']")
    public WebElement itemDashboardTittle;

    @FindBy(how = How.XPATH, using = "//li[@class='nav-item dropdown no-arrow']//a//span")
    public WebElement itemUsername;

    @FindBy(how = How.XPATH, using = "//div[@class='card-body']/div/div/div[text()='Total']")
    public WebElement itemResumeDocumentsTotal;

    public LoginPage Logout() throws InterruptedException{
        listSettings.click();
        Thread.sleep(1000);
        itemLogout.click();
        return GetInstance(LoginPage.class);
    }

    public boolean isDashboardLoad(){
        return DriverContext.isItemDisplayed(itemDashboardTittle);
    }

    public String getUserLogged(){
        return itemUsername.getText();
    }

    public boolean isUserLogged(){
        return DriverContext.isItemDisplayed(itemUsername);
    }

    public boolean isResumeDocumentsTotalDisplayed() {
        DriverContext.WaitForElementVisible(itemResumeDocumentsTotal);
        return DriverContext.isItemDisplayed(itemResumeDocumentsTotal);
    }

}
