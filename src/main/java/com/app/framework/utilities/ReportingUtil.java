package com.app.framework.utilities;

import java.sql.Connection;
import java.util.Hashtable;

public class ReportingUtil {

    public static void CreateTestCycle(Connection connection){

        //params
        Hashtable table = new Hashtable();
        table.put("AUT","ExecuteAutomationWebApp");
        table.put("ExecutedBy","Jose");
        table.put("BuildNo","1.0");
        table.put("ApplicationVersion","1.0");
        table.put("MachineName","Windows 10");
        table.put("TestType","1");

        DatabaseUtil.ExecuteStoreProc("sp_CreateTestCycleID",table,connection);

    }

    public static void WriteTestResult(Connection connection, String scenarioName, String featureName, String stepName, String exception, String result){
        try {
            Hashtable table = new Hashtable();
            table.put("FeatureName", featureName);
            table.put("ScenarioName", scenarioName);
            table.put("StepName", stepName);
            table.put("Exception", exception);
            table.put("Result", result);
            DatabaseUtil.ExecuteStoreProc("sp_InsertResult", table, connection);
        } catch (Exception e){}
    }
}
