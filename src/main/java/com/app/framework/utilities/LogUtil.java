package com.app.framework.utilities;

import com.app.framework.config.Settings;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class LogUtil {

    //file format for the log name
    ZonedDateTime date = ZonedDateTime.now();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyyHHMMSS");
    String fileNameFormat = date.format(formatter);

    private BufferedWriter bufferedWriter = null;

    //create log file
    public void CreateLogFile(){
        try{
            var dir = new File(Settings.LogPath);
            if (!dir.exists()) dir.mkdir();
            //Create file
            var logFile = new File(dir + "/" + fileNameFormat + ".log");
            var fileWriter = new FileWriter(logFile.getAbsoluteFile());
            bufferedWriter = new BufferedWriter(fileWriter);
        }
        catch(Exception e){
        }
    }

    //write message within the log
    public void Write(String message){
        try{
            formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy:HH_MM_SS");
            var dateFormat = date.format(formatter);
            bufferedWriter.write("[" + dateFormat + "]" + message);
            bufferedWriter.newLine();
            bufferedWriter.flush();
        }
        catch (Exception e){

        }

    }



}
