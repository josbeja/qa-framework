package com.app.framework.utilities;

import java.sql.*;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.concurrent.Callable;

public class DatabaseUtil {

    public static Connection Open(String connectionString){

        try{
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            return DriverManager.getConnection(connectionString);
        }catch (Exception e){

        }
        return null;
    }

    public static void Close(){

    }

    public static void ExecuteQuery(String query, Connection connection){
        Statement statement = null;
        try{
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

        }catch (Exception e){

        }

    }

    public static void ExecuteStoreProc(String procedureName, Hashtable parameters, Connection connection){
        try{
            int parameterLength = parameters.size();
            String paraApprender = null;
            StringBuilder builder = new StringBuilder();

            //build the parameters list to be passed in the store proc
            for (int i = 0; i < parameters.size(); i++){
                builder.append("?,");
            }

            paraApprender = builder.toString();
            paraApprender = paraApprender.substring(0,paraApprender.length()-1);

            CallableStatement stmt = connection.prepareCall("{Call " + procedureName + "(" + paraApprender + ")}");

            //creates enumeration for getting the keys for the parameters
            Enumeration params = parameters.keys();

            //iterate in all the elements till there is no keys
            while (params.hasMoreElements()){
                //Get the key from the parameters
                String paramsName = (String) params.nextElement();
                //Set parameters name and Value
                stmt.setString(paramsName, parameters.get(paramsName).toString());
            }
            //Execute query
            stmt.execute();
        }catch (Exception e){

        }
    }

}
