package com.app.framework.utilities;

import io.cucumber.datatable.DataTable;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;

import java.io.File;
import java.lang.reflect.Array;
import java.util.*;

public class CucumberUtil {

    private static List<DataCollection> _dataCollection = new ArrayList<>();

    public static  List<DataCollection> ConvertDataTableToDict(DataTable table){

        _dataCollection.clear();

        List<List<String>> data = table.cells();

        int rowNumber = 0;

        for(List<String> col : data){
            for(int colIndex=0; colIndex<col.size(); colIndex++){
                _dataCollection.add(rowNumber, new DataCollection(data.get(0).get(colIndex),col.get(colIndex), rowNumber));
            }
            rowNumber++;
        }
        return  _dataCollection;
    }

    public static String GetCellValueWithRowIndex(String columnName, int rowNumber){
        final String[] columnValue = {null};

        _dataCollection.forEach(x -> {
            if(x.ColumnName.equals(columnName) && x.RowNumber == rowNumber){
                columnValue[0] = x.ColumnValue;
            }
        });
        return columnValue[0];
    }

    private static class DataCollection{
        private String ColumnName;
        private String ColumnValue;
        private int RowNumber;

        public DataCollection(String columnName, String columnValue, int rowNumber) {
            ColumnName = columnName;
            ColumnValue = columnValue;
            RowNumber = rowNumber;
        }
    }

    private static void GenerateReportForJsonFiles(File reportOutputDirectory, List<String> jsonFiles) {
        String buildNumber = "1";
        String projectName = "report example";

        Configuration configuration = new Configuration(reportOutputDirectory, projectName);
        //configuration.setParallelTesting(false);
        //configuration.setJenkinsBasePath(jenkinsBasePath);
        //configuration.setRunWithJenkins(false);
        configuration.setBuildNumber(buildNumber);

        ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
        reportBuilder.generateReports();
    }

    public static void CreateReport(){
        File file = new File("target/");
        List<String> listJson = Arrays.asList("target/cucumber-reports/cucumber.json");

        GenerateReportForJsonFiles(file, listJson);

    }

}

