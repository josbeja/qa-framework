package com.app.framework.base;

public enum BrowserType{
    Firefox,
    Chrome,
    IE,
    RemoteChrome,
}
