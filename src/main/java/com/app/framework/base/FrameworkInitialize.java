package com.app.framework.base;

import com.app.framework.config.Settings;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class FrameworkInitialize extends Base {

    public void InitializeBrowser (BrowserType browserType){

        //WebDriver driver = null;
        RemoteWebDriver driver = null;
        switch (browserType){
            case Firefox:
            {
                driver = new ChromeDriver();
                break;
            }
            case Chrome:
            {
                System.setProperty("webdriver.chrome.driver", "D:\\Documentos\\Jose\\qa-framework\\src\\resources\\ChromeDriver.exe");
                try {
                    driver = new ChromeDriver();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            }
            case IE:
            {
                break;
            }
            case RemoteChrome:
            {
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setBrowserName("chrome");
                try {
                    driver = new RemoteWebDriver(new URL(Settings.SeleniumGridHub),capabilities);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                break;
            }

        }

        LocalDriverContext.setRemoteWebDriverThreadLocal(driver);
        //DriverContext.setDriver(driver);
        //DriverContext.Browser = new Browser(driver);
    }

}
