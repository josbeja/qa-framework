package com.app.framework.base;

import com.app.framework.config.Settings;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverContext {

    /*
    public static WebDriver Driver;

    public static Browser Browser;

    public static void setDriver(WebDriver driver) {
        Driver = driver;
    }
    */

    public static void GoToUrl(String url){
        LocalDriverContext.getRemoteWebDriver().get(url);
        DriverContext.WaitForPageToLoad();
    }

    public static void Maximize(){
        LocalDriverContext.getRemoteWebDriver().manage().window().maximize();
    }

    public static void Quit(){
        LocalDriverContext.getRemoteWebDriver().quit();
    }

    public static void WaitForPageToLoad(){
        var wait = new WebDriverWait(LocalDriverContext.getRemoteWebDriver(), 30);
        var jsExecutor = (JavascriptExecutor) LocalDriverContext.getRemoteWebDriver();

        ExpectedCondition<Boolean> jsLoad = webDriver -> ((JavascriptExecutor)LocalDriverContext.getRemoteWebDriver())
                .executeScript("return document.readyState").toString().equals("complete");

        //Get JS Ready
        boolean jsReady = jsExecutor.executeScript("return document.readyState").toString().equals("complete");

        if(!jsReady){
            wait.until(jsLoad);
        } else {
            Settings.Logs.Write("Page is ready");
        }

    }

    public static void WaitForElementVisible(final WebElement elementFindBy){
        WebDriverWait wait = new WebDriverWait(LocalDriverContext.getRemoteWebDriver(), 30);
        wait.until(ExpectedConditions.visibilityOf(elementFindBy));
    }

    public static void WaitForElementTextVisible(final WebElement elementFindBy, String text){
        WebDriverWait wait = new WebDriverWait(LocalDriverContext.getRemoteWebDriver(), 30);
        wait.until(ExpectedConditions.textToBePresentInElement(elementFindBy, text));
    }

    public static void WaitForElementTextDisplayed(final By element, String text){
        WebDriverWait wait = new WebDriverWait(LocalDriverContext.getRemoteWebDriver(), 30);
        wait.until(textDisplayed(element, text));
    }

    private static ExpectedCondition<Boolean> textDisplayed(final By elementFindBy, final String text){
        return webDriver -> webDriver.findElement(elementFindBy).getText().contains(text);
    }

    public static void WaitForElementEnabled(final By elementFindBy){
        WebDriverWait wait = new WebDriverWait(LocalDriverContext.getRemoteWebDriver(),30);
        wait.until(webDriver -> webDriver.findElement(elementFindBy)).isEnabled();
    }

    public static boolean isItemDisplayed(WebElement webElement){
        boolean isItemDisplayed = false;
        if(webElement.isDisplayed()){
            isItemDisplayed = true;
        }
        return isItemDisplayed;
    }

}
