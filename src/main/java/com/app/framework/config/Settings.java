package com.app.framework.config;

import com.app.framework.base.BrowserType;
import com.app.framework.utilities.LogUtil;

import java.sql.Connection;

public class Settings {

    //For application backend
    public static Connection AUTConnection;

    //For EARS reporting
    public static Connection ReportingConnection;

    //For application backend connection string
    public static String AUTConnectionString;
    public static String ReportingConnectionString;

    //Log path for framework
    public static String LogPath;

    //Driver type for MYSQL connectivity
    public static String DriverType;
    public static String ExcelSheetPath;
    public static String AUT;

    public static BrowserType BrowserType;
    public static String BrowserName;

    public static LogUtil Logs;
    public static String SeleniumGridHub;

}
