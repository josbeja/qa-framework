package com.app.framework.config;

import com.app.framework.base.BrowserType;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigReader {

    public static void PopulateSettings() throws IOException {
        ConfigReader reader = new ConfigReader();
        reader.ReadProperty();
    }

    public void ReadProperty() throws IOException{
        Properties p = new Properties();
        InputStream inputStream = new FileInputStream("src/main/java/com/app/framework/config/GlobalConfig.properties");
        p.load(inputStream);
        Settings.AUTConnectionString = p.getProperty("AUTConnectionString");
        Settings.ReportingConnectionString = p.getProperty("ReportingConnectionString");
        Settings.LogPath = p.getProperty("LogPath");
        Settings.DriverType = p.getProperty("DriverType");
        Settings.ExcelSheetPath = p.getProperty("ExcelSheetPath");
        Settings.AUT = p.getProperty("AUT");
        Settings.BrowserType = BrowserType.valueOf(p.getProperty("BrowserType"));
        Settings.BrowserName = p.getProperty("BrowserName");
        Settings.SeleniumGridHub = p.getProperty("SeleniumGrid");

    }
}