jsonPWrapper ({
  "Features": [
    {
      "RelativeFolder": "becaPresidenteDeLaRepublicaFeatures\\experienciaLaboral.feature",
      "Feature": {
        "Name": "Formulario de Experiencia Laboral",
        "Description": "Experiencia cuente desde la fecha minima de todos sus grados",
        "FeatureElements": [
          {
            "Name": "Registrar Experiencia con fecha menor al minimo grado",
            "Slug": "registrar-experiencia-con-fecha-menor-al-minimo-grado",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante registra experiencia con fecha menor al minimo grado",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema no permite registrar experiencia",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Registrar Experiencia con fecha igual al ultimo grado",
            "Slug": "registrar-experiencia-con-fecha-igual-al-ultimo-grado",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante registra experiencia con fecha igual al minimo grado",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema permite registrar experiencia",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Editar Experiencia con fecha menor al ultimo grado",
            "Slug": "editar-experiencia-con-fecha-menor-al-ultimo-grado",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante edita experiencia con fecha menor al minimo grado",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema no permite editar experiencia",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Editar Experiencia con fecha igual al ultimo grado",
            "Slug": "editar-experiencia-con-fecha-igual-al-ultimo-grado",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante edita experiencia con fecha igual al minimo grado",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema permite editar experiencia",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Registrar Experiencia con fecha mayor al ultimo grado",
            "Slug": "registrar-experiencia-con-fecha-mayor-al-ultimo-grado",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante registra experiencia con fecha mayor al minimo grado",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema permite editar experiencia",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Editar Experiencia con fecha mayor al ultimo grado",
            "Slug": "editar-experiencia-con-fecha-mayor-al-ultimo-grado",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante edita experiencia con fecha mayor al minimo grado",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema permite editar experiencia",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          }
        ],
        "Background": {
          "Name": "Pasos previos",
          "Description": "",
          "Steps": [
            {
              "Keyword": "Given",
              "NativeKeyword": "Given ",
              "Name": "Pantalla de Experiencia Laboral cargada correctamente",
              "StepComments": [],
              "AfterLastStepComments": []
            }
          ],
          "Tags": [],
          "Result": {
            "WasExecuted": false,
            "WasSuccessful": false,
            "WasProvided": false
          }
        },
        "Result": {
          "WasExecuted": false,
          "WasSuccessful": false,
          "WasProvided": true
        },
        "Tags": [
          "@BecaPresidenteDeLaRepublica"
        ]
      },
      "Result": {
        "WasExecuted": false,
        "WasSuccessful": false,
        "WasProvided": true
      }
    },
    {
      "RelativeFolder": "becaPresidenteDeLaRepublicaFeatures\\informacionAcademica.feature",
      "Feature": {
        "Name": "Formulario de Información Academica",
        "Description": "Información academica del Postulante y documentación adjunta",
        "FeatureElements": [
          {
            "Name": "Requisito minimo Bachiller",
            "Slug": "requisito-minimo-bachiller",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante registra bachiller como unica información academica",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "Postulante da click en seguir",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema permite continuar",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Requisito minimo titulo tecnico profesional",
            "Slug": "requisito-minimo-titulo-tecnico-profesional",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante registra titulo tecnico profesional como unica información academica",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "Postulante da click en seguir",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema permite continuar",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Requisito minimo Titulo Profesional",
            "Slug": "requisito-minimo-titulo-profesional",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante registra Titulo Profesional como unica información academica",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "Postulante da click en seguir",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema permite continuar",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Requisito minimo Licenciatura",
            "Slug": "requisito-minimo-licenciatura",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante registra Licenciatura como unica información academica",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "Postulante da click en seguir",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema permite continuar",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Requisito minimo Maestria",
            "Slug": "requisito-minimo-maestria",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante registra Maestria como unica información academica",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "Postulante da click en seguir",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema permite continuar",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Mas de un registro en información academica",
            "Slug": "mas-de-un-registro-en-informacion-academica",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante registra Bachiller como información academica",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "Postulante registra Maestria como información academica",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "Postulante da click en seguir",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema permite continuar",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Retornar a Editar registro unico una vez cargada experiencia laboral",
            "Slug": "retornar-a-editar-registro-unico-una-vez-cargada-experiencia-laboral",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante vuelve a la pantalla de información academica",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "Postulante edita registro",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema no permite editar registro",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Retornar a Editar registros con menor fecha una vez cargada experiencia laboral",
            "Slug": "retornar-a-editar-registros-con-menor-fecha-una-vez-cargada-experiencia-laboral",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante vuelve a la pantalla de información academica",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "Postulante edita registro con menor fecha",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema no permite editar registro",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Postulante editar registro con mayor fecha",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "Sistema permite editar registro",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Retornar a eliminar unico registro una vez cargada experienca laboral",
            "Slug": "retornar-a-eliminar-unico-registro-una-vez-cargada-experienca-laboral",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante vuelve a la pantalla de información academica",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "Postulante elimina unico registro",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema no permite eliminar registro",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "Sistema muestra mensaje de advertencia",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Retornar a eliminar registros una vez cargada experienca laboral",
            "Slug": "retornar-a-eliminar-registros-una-vez-cargada-experienca-laboral",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Postulante vuelve a la pantalla de información academica",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "When",
                "NativeKeyword": "When ",
                "Name": "Postulante elimina registros exceptuando el de menor fecha",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Sistema permite eliminar registros",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "Sistema muestra mensaje confirmando eliminación",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          }
        ],
        "Background": {
          "Name": "Pasos previos",
          "Description": "",
          "Steps": [
            {
              "Keyword": "Given",
              "NativeKeyword": "Given ",
              "Name": "Pantalla de Información academica cargada correctamente",
              "StepComments": [],
              "AfterLastStepComments": []
            }
          ],
          "Tags": [],
          "Result": {
            "WasExecuted": false,
            "WasSuccessful": false,
            "WasProvided": false
          }
        },
        "Result": {
          "WasExecuted": false,
          "WasSuccessful": false,
          "WasProvided": true
        },
        "Tags": [
          "@BecaPresidenteDeLaRepublica"
        ]
      },
      "Result": {
        "WasExecuted": false,
        "WasSuccessful": false,
        "WasProvided": true
      }
    },
    {
      "RelativeFolder": "becaPresidenteDeLaRepublicaFeatures\\moduloValidación.feature",
      "Feature": {
        "Name": "Modulo de validación",
        "Description": "Módulo de Validación de Expedientes presentados en la\r\nFase de Postulación de la Beca Presidente de la República - Convocatoria 2020",
        "FeatureElements": [
          {
            "Name": "Validar cabecera información general",
            "Slug": "validar-cabecera-informacion-general",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Se ingresa con el usuario y contraseña",
                "TableArgument": {
                  "HeaderRow": [
                    "usuario",
                    "contraseña"
                  ],
                  "DataRows": [
                    [
                      "48318579",
                      "Pronabec2019"
                    ]
                  ]
                },
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Se verifica la cabecera de Información General",
                "TableArgument": {
                  "HeaderRow": [
                    "Cabecera"
                  ],
                  "DataRows": [
                    [
                      "N° de expediente (generado)"
                    ],
                    [
                      "Número de DNI"
                    ],
                    [
                      "Apellidos y Nombres"
                    ],
                    [
                      "Sexo"
                    ],
                    [
                      "Lengua Materna"
                    ],
                    [
                      "Dirección según RENIEC"
                    ],
                    [
                      "Domicilio actual"
                    ],
                    [
                      "Edad a la fecha de publicación de las bases"
                    ],
                    [
                      "Comunidad o Población a la que pertenece (de ser el caso)"
                    ],
                    [
                      "Región, provincia y distrito de la comunidad o población a la que pertenecer (de ser el caso)"
                    ],
                    [
                      "Tipo y sub tipo de discapacidad (de ser el caso)"
                    ]
                  ]
                },
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Validar Información del programa al que postula",
            "Slug": "validar-informacion-del-programa-al-que-postula",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Given",
                "NativeKeyword": "Given ",
                "Name": "Se ingresa con el usuario y contraseña",
                "TableArgument": {
                  "HeaderRow": [
                    "usuario",
                    "contraseña"
                  ],
                  "DataRows": [
                    [
                      "48318579",
                      "Pronabec2019"
                    ]
                  ]
                },
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Se verifica la cabecera de Información General",
                "TableArgument": {
                  "HeaderRow": [
                    "Tipo de Programa"
                  ],
                  "DataRows": [
                    [
                      "País de destino"
                    ],
                    [
                      "IES de destino"
                    ],
                    [
                      "Nombre del programa de destino"
                    ],
                    [
                      "Area de conocimiento"
                    ],
                    [
                      "Costo del programa"
                    ],
                    [
                      "Duración del programa"
                    ],
                    [
                      "Fecha de inicio y fin de los estudios"
                    ]
                  ]
                },
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Validar requisitos y documentos obligatorios y opcionales",
            "Slug": "validar-requisitos-y-documentos-obligatorios-y-opcionales",
            "Description": "Given\r\n  |usuario|contraseña|\r\n  |48318579|Pronabec2019|",
            "Steps": [
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Se verifica los documentos obligatorios",
                "TableArgument": {
                  "HeaderRow": [
                    "documentos obligatorios"
                  ],
                  "DataRows": [
                    [
                      "Estudios de pregrado o posgrado"
                    ],
                    [
                      "Rendimiento académico"
                    ]
                  ]
                },
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "Se verifica los documentos opcionales",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          }
        ],
        "Background": {
          "Name": "",
          "Description": "",
          "Steps": [
            {
              "Keyword": "Given",
              "NativeKeyword": "Given ",
              "Name": "La URL de prueba",
              "StepComments": [],
              "AfterLastStepComments": []
            },
            {
              "Keyword": "Then",
              "NativeKeyword": "Then ",
              "Name": "Se carga la pagina de prueba",
              "TableArgument": {
                "HeaderRow": [
                  "URL"
                ],
                "DataRows": [
                  [
                    "https://prueba10.pronabec.gob.pe/"
                  ]
                ]
              },
              "StepComments": [],
              "AfterLastStepComments": []
            }
          ],
          "Tags": [],
          "Result": {
            "WasExecuted": false,
            "WasSuccessful": false,
            "WasProvided": false
          }
        },
        "Result": {
          "WasExecuted": false,
          "WasSuccessful": false,
          "WasProvided": true
        },
        "Tags": [
          "@BecaPresidenteDeLaRepublica"
        ]
      },
      "Result": {
        "WasExecuted": false,
        "WasSuccessful": false,
        "WasProvided": true
      }
    },
    {
      "RelativeFolder": "exampleFeatures\\CheckDashboardItems.feature",
      "Feature": {
        "Name": "CheckDashboardItems",
        "Description": "Verify if all items of dashboard are successful loaded",
        "FeatureElements": [
          {
            "Name": "Verify TOTAL documents",
            "Slug": "verify-total-documents",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I verify if TOTAL is displayed",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Verify FACTURAS PENDIENTES PARA CANCELAR",
            "Slug": "verify-facturas-pendientes-para-cancelar",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I verify if FACTURAS PENDIENTES PARA CANCELAR is displayed",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Verify BOLETAS PENDIENTES",
            "Slug": "verify-boletas-pendientes",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I verify if BOLETAS PENDIENTES is displayed",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Verify BOLETAS PENDIENTES PARA CANCELAR",
            "Slug": "verify-boletas-pendientes-para-cancelar",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I verify if BOLETAS PENDIENTES PARA CANCELAR is displayed",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Verify Resumen por día",
            "Slug": "verify-resumen-por-dia",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I verify if Resumen por día is displayed",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Verify Resumen por tipo",
            "Slug": "verify-resumen-por-tipo",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I verify if Resumen por tipo is displayed",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Verify Resumen por estado",
            "Slug": "verify-resumen-por-estado",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I verify if Resumen por estado is displayed",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Verify Notificaciones",
            "Slug": "verify-notificaciones",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I verify if Notificaciones is displayed",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          }
        ],
        "Background": {
          "Name": "",
          "Description": "",
          "Steps": [
            {
              "Keyword": "Given",
              "NativeKeyword": "Given ",
              "Name": "I ensure the application is opened",
              "StepComments": [],
              "AfterLastStepComments": []
            },
            {
              "Keyword": "Then",
              "NativeKeyword": "Then ",
              "Name": "I enter Ruc, User and Password",
              "TableArgument": {
                "HeaderRow": [
                  "Ruc",
                  "UserName",
                  "Password"
                ],
                "DataRows": [
                  [
                    "11111111111",
                    "admin",
                    "admin"
                  ]
                ]
              },
              "StepComments": [],
              "AfterLastStepComments": []
            },
            {
              "Keyword": "Then",
              "NativeKeyword": "Then ",
              "Name": "I click login button (success login)",
              "StepComments": [],
              "AfterLastStepComments": []
            },
            {
              "Keyword": "And",
              "NativeKeyword": "And ",
              "Name": "I ensure the Dashboard is opened",
              "StepComments": [],
              "AfterLastStepComments": []
            }
          ],
          "Tags": [],
          "Result": {
            "WasExecuted": false,
            "WasSuccessful": false,
            "WasProvided": false
          }
        },
        "Result": {
          "WasExecuted": false,
          "WasSuccessful": false,
          "WasProvided": true
        },
        "Tags": [
          "@LoginFeatures"
        ]
      },
      "Result": {
        "WasExecuted": false,
        "WasSuccessful": false,
        "WasProvided": true
      }
    },
    {
      "RelativeFolder": "exampleFeatures\\User.feature",
      "Feature": {
        "Name": "Feature2-UserFeature",
        "Description": "This feature is responsible for testing new User creation, edit user, deactivate user, activate user, promove user, degrade user",
        "FeatureElements": [
          {
            "Name": "Scenario1-User",
            "Slug": "scenario1-user",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I click Users link",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I click new user button",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I enter the new user, password, confirm password",
                "StepComments": [
                  {
                    "Text": "#Then I select the test company \"uno\""
                  }
                ],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I click Save button",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I verify if the new user was created",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Scenario2-User",
            "Slug": "scenario2-user",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I click Users link",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I click new user button",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I enter the new user, password, confirm password",
                "StepComments": [
                  {
                    "Text": "#Then I select the test company \"uno\""
                  }
                ],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I click Save button",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I verify if the new user was created",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          }
        ],
        "Background": {
          "Name": "",
          "Description": "",
          "Steps": [
            {
              "Keyword": "Given",
              "NativeKeyword": "Given ",
              "Name": "I ensure the application is opened",
              "StepComments": [],
              "AfterLastStepComments": []
            },
            {
              "Keyword": "Then",
              "NativeKeyword": "Then ",
              "Name": "I enter Ruc, User and Password",
              "TableArgument": {
                "HeaderRow": [
                  "Ruc",
                  "UserName",
                  "Password"
                ],
                "DataRows": [
                  [
                    "11111111111",
                    "admin",
                    "admin"
                  ]
                ]
              },
              "StepComments": [],
              "AfterLastStepComments": []
            },
            {
              "Keyword": "Then",
              "NativeKeyword": "Then ",
              "Name": "I click login button (success login)",
              "StepComments": [],
              "AfterLastStepComments": []
            },
            {
              "Keyword": "And",
              "NativeKeyword": "And ",
              "Name": "I ensure the Dashboard is opened",
              "StepComments": [],
              "AfterLastStepComments": []
            }
          ],
          "Tags": [],
          "Result": {
            "WasExecuted": false,
            "WasSuccessful": false,
            "WasProvided": false
          }
        },
        "Result": {
          "WasExecuted": false,
          "WasSuccessful": false,
          "WasProvided": true
        },
        "Tags": [
          "@UserFeatures"
        ]
      },
      "Result": {
        "WasExecuted": false,
        "WasSuccessful": false,
        "WasProvided": true
      }
    },
    {
      "RelativeFolder": "exampleFeatures\\Login.feature",
      "Feature": {
        "Name": "LoginFeature",
        "Description": "This feature is responsible for testing all the scenarios for Login.",
        "FeatureElements": [
          {
            "Name": "Login with valid credentials",
            "Slug": "login-with-valid-credentials",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I enter Ruc, User and Password",
                "TableArgument": {
                  "HeaderRow": [
                    "Ruc",
                    "UserName",
                    "Password"
                  ],
                  "DataRows": [
                    [
                      "11111111111",
                      "admin",
                      "admin"
                    ]
                  ]
                },
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I click login button (success login)",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I ensure the Dashboard is opened",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I should see the dashboard page with the user name",
                "TableArgument": {
                  "HeaderRow": [
                    "UserName"
                  ],
                  "DataRows": [
                    [
                      "admin"
                    ]
                  ]
                },
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          },
          {
            "Name": "Login with invalid credentials",
            "Slug": "login-with-invalid-credentials",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I enter Ruc, User and Password",
                "TableArgument": {
                  "HeaderRow": [
                    "Ruc",
                    "UserName",
                    "Password"
                  ],
                  "DataRows": [
                    [
                      "11111111111",
                      "admin",
                      "noadmin"
                    ]
                  ]
                },
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I click login button (fail login)",
                "StepComments": [],
                "AfterLastStepComments": []
              },
              {
                "Keyword": "And",
                "NativeKeyword": "And ",
                "Name": "I should see the error message \"Credenciales no válidas.\"",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          }
        ],
        "Background": {
          "Name": "",
          "Description": "",
          "Steps": [
            {
              "Keyword": "Given",
              "NativeKeyword": "Given ",
              "Name": "I ensure the application is opened",
              "StepComments": [],
              "AfterLastStepComments": []
            }
          ],
          "Tags": [],
          "Result": {
            "WasExecuted": false,
            "WasSuccessful": false,
            "WasProvided": false
          }
        },
        "Result": {
          "WasExecuted": false,
          "WasSuccessful": false,
          "WasProvided": true
        },
        "Tags": [
          "@LoginFeatures"
        ]
      },
      "Result": {
        "WasExecuted": false,
        "WasSuccessful": false,
        "WasProvided": true
      }
    },
    {
      "RelativeFolder": "exampleFeatures\\test.feature",
      "Feature": {
        "Name": "testFeature",
        "Description": "This feature is responsible for testing new User creation, edit user, deactivate user, activate user, promove user, degrade user",
        "FeatureElements": [
          {
            "Name": "Scenario1-User",
            "Slug": "scenario1-user",
            "Description": "",
            "Steps": [
              {
                "Keyword": "Then",
                "NativeKeyword": "Then ",
                "Name": "I click Users link",
                "StepComments": [],
                "AfterLastStepComments": []
              }
            ],
            "Tags": [],
            "Result": {
              "WasExecuted": false,
              "WasSuccessful": false,
              "WasProvided": true
            }
          }
        ],
        "Background": {
          "Name": "",
          "Description": "",
          "Steps": [
            {
              "Keyword": "Given",
              "NativeKeyword": "Given ",
              "Name": "I ensure the application is opened",
              "StepComments": [],
              "AfterLastStepComments": []
            },
            {
              "Keyword": "Then",
              "NativeKeyword": "Then ",
              "Name": "I enter Ruc, User and Password",
              "TableArgument": {
                "HeaderRow": [
                  "Ruc",
                  "UserName",
                  "Password"
                ],
                "DataRows": [
                  [
                    "11111111111",
                    "admin",
                    "admin"
                  ]
                ]
              },
              "StepComments": [],
              "AfterLastStepComments": []
            },
            {
              "Keyword": "Then",
              "NativeKeyword": "Then ",
              "Name": "I click login button (success login)",
              "StepComments": [],
              "AfterLastStepComments": []
            },
            {
              "Keyword": "And",
              "NativeKeyword": "And ",
              "Name": "I ensure the Dashboard is opened",
              "StepComments": [],
              "AfterLastStepComments": []
            }
          ],
          "Tags": [],
          "Result": {
            "WasExecuted": false,
            "WasSuccessful": false,
            "WasProvided": false
          }
        },
        "Result": {
          "WasExecuted": false,
          "WasSuccessful": false,
          "WasProvided": true
        },
        "Tags": [
          "@testScenario"
        ]
      },
      "Result": {
        "WasExecuted": false,
        "WasSuccessful": false,
        "WasProvided": true
      }
    }
  ],
  "Summary": {
    "Tags": [
      {
        "Tag": "@BecaPresidenteDeLaRepublica",
        "Total": 19,
        "Passing": 0,
        "Failing": 0,
        "Inconclusive": 19
      },
      {
        "Tag": "@LoginFeatures",
        "Total": 10,
        "Passing": 0,
        "Failing": 0,
        "Inconclusive": 10
      },
      {
        "Tag": "@UserFeatures",
        "Total": 2,
        "Passing": 0,
        "Failing": 0,
        "Inconclusive": 2
      },
      {
        "Tag": "@testScenario",
        "Total": 1,
        "Passing": 0,
        "Failing": 0,
        "Inconclusive": 1
      }
    ],
    "Folders": [
      {
        "Folder": "becaPresidenteDeLaRepublicaFeatures",
        "Total": 19,
        "Passing": 0,
        "Failing": 0,
        "Inconclusive": 19
      },
      {
        "Folder": "exampleFeatures",
        "Total": 13,
        "Passing": 0,
        "Failing": 0,
        "Inconclusive": 13
      }
    ],
    "NotTestedFolders": [
      {
        "Folder": "becaPresidenteDeLaRepublicaFeatures",
        "Total": 0,
        "Passing": 0,
        "Failing": 0,
        "Inconclusive": 0
      },
      {
        "Folder": "exampleFeatures",
        "Total": 0,
        "Passing": 0,
        "Failing": 0,
        "Inconclusive": 0
      }
    ],
    "Scenarios": {
      "Total": 32,
      "Passing": 0,
      "Failing": 0,
      "Inconclusive": 32
    },
    "Features": {
      "Total": 7,
      "Passing": 0,
      "Failing": 0,
      "Inconclusive": 7
    }
  },
  "Configuration": {
    "SutName": "qaLivinDoc",
    "SutVersion": "0.1",
    "GeneratedOn": "3 julio 2020 15:20:21"
  }
});