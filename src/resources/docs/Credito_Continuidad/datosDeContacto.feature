@formulario3
Feature: Validación de datos de contacto
  Subsección “Datos de contacto”, donde se muestra información 
  que debe digitar o seleccionar el postulante
  
  Background: Pasos previos
    Given Un Postulante carga a la web Credito continuidad de estudios
    When El Postulante ingresa con el DNI y contraseña validos
    Then Postulante ingreso satisfactoriamente
    And Lleno los datos generales satisfactoriamente
    And Lleno los datos de los padres satisfactoriamente 
    And Formulario Validación de datos de contacto cargo satisfactoriamente
  
  Scenario: Validar datos del Postulante precargados de RENIEC
    Given Validamos dirección precargada desde RENIEC
    Then And Validamos que exista el boton editar

  Scenario: Validar aviso para actualizar dirección
    Given Vericamos los datos precargados desde RENIEC
    Then Validamos que se muestre el aviso
      """
      Si tu dirección actual no es la que figura, actualiza tu dirección
      """

  Scenario: Validar campos a llenar por el Postulante
    Given Correo electronico se debe digitar y es obligatorio
    Then Numero de telefono de referencia calular se debe digitar y es obligatorio
    And Numero de telefono fijo se debe digitar y es opcional
    And Estado civil del postulante es una lista desplegable
    And Numero de hijos se debe digitar
     
  Scenario: Validar DNI autocarga del DNI del conyuge 
    Given Postulante selecciona casado en estado civil
    Then DNI del conyuge carga automaticamente desde RENIEC

  Scenario: Actualizar dirección actual
    Given seleccionar el boton editar
    Then Cargo el formulario Direccion actual
    Then Digitamos la nueva Direccion
    And Digitamos la referencia
    Then Seleccionamos el departamento
    And Seleccionamos la provincia
    And Seleccionamos el distrito
    Then Click en el boton aceptar
    And Validamos que la nueva dirección se actualice en el formulario

  Scenario: Validar datos del Representante para menores de edad
    Given Formulario Datos del representante es mostrado
    Then Campo tipo de representante debe mostrar tres radio buttons
    And Numero de DNI debe ser un label
    And Apellidos y nombres debe ser un label
    And Telefono se debe digitar en un textbox
    And Correo electronico se debe digitar en dos textbox

  Scenario: Validar datos del Representante escogiendo padre o madre
    Given Formulario Datos del representante es mostrado
    When Seleccionamos padre o madre como representante
    Then Se debe completar los datos del reprentante basado en el formulario datos de los padres

  Scenario: Validar datos del Representante escogiendo apoderado
    Given Formulario Datos del representante es mostrado
    When Seleccionamos apoderado como representante
    And Digitamos el DNI del apoderado
    Then Los datos del apoderado son cargados desde RENIEC