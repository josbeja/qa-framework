@formulario5
Feature: Validación de datos del cotitular
  
  Background: Pasos previos
    Given Un Postulante carga a la web Credito continuidad de estudios
    When El Postulante ingresa con el DNI y contraseña validos
    Then Postulante ingreso satisfactoriamente
    And Lleno los datos generales satisfactoriamente
    And Lleno los datos de los padres satisfactoriamente 
    And Lleno los datos de contacto satisfactoriamente
    And Lleno los datos adicionales satisfactoriamente
    And Cargo satisfactoriamente el formulario de datos del cotitular

  Scenario: Carga del datos del cotitular menor de 18 años desde RENIEC
    Given Se digita el DNI del cotitular menor de 18 años
    Then El sistema no permite registrar el cotitular

  Scenario: Carga del datos del cotitular mayor de 70 años desde RENIEC
    Given Se digita el DNI del cotitular mayor de 70 años
    Then El sistema no permite registrar el cotitular

  Scenario: Carga del datos del cotitular valido casado desde RENIEC
    Given Se digita el DNI del cotitular valido casado
    Then Se cargan los datos del cotitular
    And Se cargan los datos del conyugue
    Then Se digita la dirección del cotitular
    Then Seleccionar el boton editar
    Then Cargo el formulario Direccion actual
    Then Digitamos la nueva Direccion
    And Digitamos la referencia
    Then Seleccionamos el departamento
    And Seleccionamos la provincia
    And Seleccionamos el distrito
    Then Click en el boton aceptar
    And Validamos que la nueva dirección se actualice en el formulario

  Scenario: Validación de cotitular distinto al Postulante
    Given Se digita el DNI del Postulante
    Then El sistema genera un mensaje de error

  Scenario: Validación de cotitular distinto al conyugue del Postulante
    Given Se digita el DNI del conyugue del Postulante
    Then El sistema genera un mensaje de error