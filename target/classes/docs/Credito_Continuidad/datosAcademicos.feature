@formulario6
Feature: Validación de datos academicos
  
  Background: Pasos previos
    Given Un Postulante carga a la web Credito continuidad de estudios
    When El Postulante ingresa con el DNI y contraseña validos
    Then Postulante ingreso satisfactoriamente
    And Lleno los datos generales satisfactoriamente
    And Lleno los datos de los padres satisfactoriamente 
    And Lleno los datos de contacto satisfactoriamente
    And Lleno los datos adicionales satisfactoriamente
    And Lleno los datos datos del cotitular satisfactoriamente
    And Cargo satisfactoriamente el formulario de datos academicos

  Scenario: Validar primer desplegable con 2 opciones de matricula
    Given El primer desplegable tiene el titulo
      """
      Último semestre o año académico cursado
      """
    Then Se muestran 2 opciones de matricula
      |Opciones de matricula|
      |2019 II o año académico 2019|
      |2020 I o año académico 2020 (en curso)|

  Scenario: Validar información pre registrada para estudiantes 2019 II o año academico 2019
    Given Postulante selecciona la opcion de matricula 2019 II o año academico 2019
    Then Se valida que el sistema autocomplete los campos

  Scenario: Validar mensaje antes de la edición de información pre registrada para estudiantes 2019 II o año academico 2019
    Given Postulante selecciona la opcion de matricula 2019 II o año academico 2019
    Then Se valida que el sistema autocomplete los campos
    Then Se da click en el boton Editar/modificar
    Then Validar mensaje:
      """
      Usted está a punto de modificar sus datos académicos, 
      deberá luego adjuntar documentación de sustento en el Paso 3: Cargar documentos
      """

  Scenario: Validar formulario de edición de información
    Given Postulante selecciona la opcion de matricula 2019 II o año academico 2019
    When Selecciona el boton editar/ modificar
    Then Validar opcion Tipo institución como lista desplegable
    And Validar opcion institución educativa como lista desplegable
    And Validar opcion sede como lista desplegable
    And Validar opcion carrera como lista desplegable
    Then Validar opcion regimen de estudios como lista desplegable
    And Las opciones de regimen de estudios pueden ser:
      |Regimen de estudios|
      |Semestral: 10 semestres a 14 semestres|
      |Anual: 5 años a 7 años|
    Then Validar opcion Ciclos academicos matriculados al 2019 II como lista desplegable
    And Las opciones de ciclos academicos matricula al 2019 II pueden ser:
      |ciclos academicos|
      |Semestral: 4 semestres a 13 semestres|
      |Anual: 2 años a 6 años|
    Then Validar opcion orden de merito como lista desplegable
    And Mostrar boton con la siguiente información:
      """
      El orden de mérito es referente al último semestre académico 
      cursado y completado (2019-II) para el caso de regímenes de estudios semestrales; 
      o al último año académico cursado y completado (2019), para regímenes anuales.
      """

  Scenario: Validar información pre registrada para estudiantes 2020 I o año académico 2020
    Given Postulante selecciona la opcion de matricula 2020 I o año académico 2020
    Then Validar opcion Tipo institución como lista desplegable
    And Validar opcion institución educativa como lista desplegable
    And Validar opcion sede como lista desplegable
    And Validar opcion carrera como lista desplegable
    Then Validar opcion regimen de estudios como lista desplegable
    And Las opciones de regimen de estudios pueden ser:
      |Regimen de estudios|
      |Semestral: 10 semestres a 14 semestres|
      |Anual: 5 años a 7 años|
    Then Validar opcion Ciclos academicos matriculados al 2019 II como lista desplegable
    And Las opciones de ciclos academicos matricula al 2019 II pueden ser:
      |ciclos academicos|
      |Semestral: 4 semestres a 13 semestres|
      |Anual: 2 años a 6 años|
    Then Validar opcion orden de merito como lista desplegable
    And Mostrar boton con la siguiente información:
      """
      El orden de mérito es referente al último semestre académico 
      cursado y completado (2019-II) para el caso de regímenes de estudios semestrales; 
      o al último año académico cursado y completado (2019), para regímenes anuales.
      """

