$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/com/app/test/features/becaPresidenteDeLaRepublicaFeatures/experienciaLaboral.feature");
formatter.feature({
  "name": "Formulario de Experiencia Laboral",
  "description": "  Experiencia cuente desde la fecha minima de todos sus grados",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Experiencia Laboral cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Registrar Experiencia con fecha menor al minimo grado",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante registra experiencia con fecha menor al minimo grado",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema no permite registrar experiencia",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Experiencia Laboral cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Registrar Experiencia con fecha igual al ultimo grado",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante registra experiencia con fecha igual al minimo grado",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema permite registrar experiencia",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Experiencia Laboral cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Editar Experiencia con fecha menor al ultimo grado",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante edita experiencia con fecha menor al minimo grado",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema no permite editar experiencia",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Experiencia Laboral cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Editar Experiencia con fecha igual al ultimo grado",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante edita experiencia con fecha igual al minimo grado",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema permite editar experiencia",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Experiencia Laboral cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Registrar Experiencia con fecha mayor al ultimo grado",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante registra experiencia con fecha mayor al minimo grado",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema permite editar experiencia",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Experiencia Laboral cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Editar Experiencia con fecha mayor al ultimo grado",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante edita experiencia con fecha mayor al minimo grado",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema permite editar experiencia",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/java/com/app/test/features/becaPresidenteDeLaRepublicaFeatures/informacionAcademica.feature");
formatter.feature({
  "name": "Formulario de Información Academica",
  "description": "  Información academica del Postulante y documentación adjunta",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Información academica cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Requisito minimo Bachiller",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante registra bachiller como unica información academica",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Postulante da click en seguir",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema permite continuar",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Información academica cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Requisito minimo titulo tecnico profesional",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante registra titulo tecnico profesional como unica información academica",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Postulante da click en seguir",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema permite continuar",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Información academica cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Requisito minimo Titulo Profesional",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante registra Titulo Profesional como unica información academica",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Postulante da click en seguir",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema permite continuar",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Información academica cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Requisito minimo Licenciatura",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante registra Licenciatura como unica información academica",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Postulante da click en seguir",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema permite continuar",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Información academica cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Requisito minimo Maestria",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante registra Maestria como unica información academica",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Postulante da click en seguir",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema permite continuar",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Información academica cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Mas de un registro en información academica",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante registra Bachiller como información academica",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Postulante registra Maestria como información academica",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Postulante da click en seguir",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema permite continuar",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Información academica cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Retornar a Editar registro unico una vez cargada experiencia laboral",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante vuelve a la pantalla de información academica",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Postulante edita registro",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema no permite editar registro",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Información academica cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Retornar a Editar registros con menor fecha una vez cargada experiencia laboral",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante vuelve a la pantalla de información academica",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Postulante edita registro con menor fecha",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema no permite editar registro",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Postulante editar registro con mayor fecha",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema permite editar registro",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Información academica cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Retornar a eliminar unico registro una vez cargada experienca laboral",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante vuelve a la pantalla de información academica",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Postulante elimina unico registro",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema no permite eliminar registro",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema muestra mensaje de advertencia",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "Pasos previos",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Pantalla de Información academica cargada correctamente",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Retornar a eliminar registros una vez cargada experienca laboral",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Postulante vuelve a la pantalla de información academica",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Postulante elimina registros exceptuando el de menor fecha",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema permite eliminar registros",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Sistema muestra mensaje confirmando eliminación",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/java/com/app/test/features/becaPresidenteDeLaRepublicaFeatures/moduloValidaci%C3%B3n.feature");
formatter.feature({
  "name": "Modulo de validación",
  "description": "  Módulo de Validación de Expedientes presentados en la\n  Fase de Postulación de la Beca Presidente de la República - Convocatoria 2020",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "La URL de prueba",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Se carga la pagina de prueba",
  "rows": [
    {},
    {}
  ],
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Validar cabecera información general",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Se ingresa con el usuario y contraseña",
  "rows": [
    {},
    {}
  ],
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Se verifica la cabecera de Información General",
  "rows": [
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {}
  ],
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "La URL de prueba",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Se carga la pagina de prueba",
  "rows": [
    {},
    {}
  ],
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Validar Información del programa al que postula",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Se ingresa con el usuario y contraseña",
  "rows": [
    {},
    {}
  ],
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Se verifica la cabecera de Información General",
  "rows": [
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {}
  ],
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "La URL de prueba",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Se carga la pagina de prueba",
  "rows": [
    {},
    {}
  ],
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.scenario({
  "name": "Validar requisitos y documentos obligatorios y opcionales",
  "description": "    Given\n      |usuario|contraseña|\n      |48318579|Pronabec2019|",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@BecaPresidenteDeLaRepublica"
    }
  ]
});
formatter.step({
  "name": "Se verifica los documentos obligatorios",
  "rows": [
    {},
    {},
    {}
  ],
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "Se verifica los documentos opcionales",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.after({
  "status": "passed"
});
});